// ---------------------------------------------------
//
// humedad.cpp sensor de humedad
//
// ---------------------------------------------------
#include "humedad.h"

// constructor por defecto
Humedad::Humedad()
{}
//
// constructor parametrizado
// ADS, valor 0 por ciento y 100 por ciento para la calibracion
//
Humedad::Humedad(Adafruit_ADS1115 ads1115v, const int valor0v,const int valor100v, const int pinAnalogicoV)
    : ads1115(ads1115v),valorCeroPorCiento(valor0v), valorCienPorCiento(valor100v), pinAnalogico(pinAnalogicoV)
{}

//
//  medir()->Z,Z
//
void Humedad::medir(){
    //guardamos la lectura
    int16_t lecturaAnalogica = (*this).ads1115.readADC_SingleEnded((*this).pinAnalogico); 
    // calculamos el porcentaje de humedad
    (*this).porcentaje = 100 * (*this).valorCeroPorCiento / ((*this).valorCeroPorCiento - (*this).valorCienPorCiento)
             - lecturaAnalogica * 100 / ((*this).valorCeroPorCiento - (*this).valorCienPorCiento);

    // comprobamos si da menos que 0 o mas que 100 para no dar valores erroneos
    if ((*this).porcentaje <= 0) {
      (*this).porcentaje= 0;
    } else if ((*this).porcentaje >= 100) {
      (*this).porcentaje = 100;
    }
}
// getters de los atributos del sensor de humedad
double Humedad::getPorcentaje(){return (*this).porcentaje;}
