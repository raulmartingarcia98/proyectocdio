#include <dummy.h>


//Autor: Grupo 11.
//Fecha: 22/10/2019.
//Descripción: Codigo completo del Arduino.

//--------------------------------
//----------LIBRERIAS-------------

#define ADS
#include <Adafruit_ADS1015.h>


#include "humedad.h"
#include "temperatura.h"
#include "salinidad.h"
#include "iluminacion.h"
#include "GPS.h"
#include "Acelerometro.h"
#include "Wifi.h"

//--------------------------------
//----------CONSTANTES------------

#define PRINT_DEBUG_MESSAGES

Adafruit_ADS1115 ads1115(0x48); // construct an ads1115 at address 0x48
Temperatura t;
Salinidad s;
Humedad h;
Iluminacion i;
GPS * g;
Wifi w;
Acelerometro a;

//***WIFI***
const char WiFiSSID[] = "HUAWEI_Y635-L01_93F2";
const char WiFiPSK[] = "b0325548";
const char Server_Host[] = "dweet.io";

const int Server_HttpPort = 80;

const char Rest_Host[] = "dweet.io";
String MyWriteAPIKey = "PruebaGTIG11"; // Escribe la clave de tu canal Dweet

WiFiClient client;

//***WIFI****/

TinyGPSPlus gps; // Definimos el objeto gps

#define RX_PIN  12 // GPS RXI
#define TX_PIN  13 // GPS TX0
#define INIT_PIN 15 // Pin para  Inicializar el GPS
#define GPS_BAUD  4800  //  velocidad de comunicación serie 
#define MPU9250_ADDRESS  0x68 // direccion del acelerometro
#define    ACC_FULL_SCALE_2_G        0x00  //Configurar selección d'escala acceleròmetre en els bits 3 i 4 (00)
#define    ACC_FULL_SCALE_4_G        0x08  // (01)
#define    ACC_FULL_SCALE_8_G        0x10  // (10)
#define    ACC_FULL_SCALE_16_G       0x18  // (11)

SoftwareSerial ss(RX_PIN, TX_PIN); // Creamos una UART software para comunicación con el GPS


//--------------------------------
//----------SETUP-----------------

void handleInterrupt(){
  a.limpiarInterrupcion();
  Serial.println("INTERRUPT=========================================================");
  
}
void setup() {
  
  Serial.begin(9600);
  Serial.println("Inicializando...");
  ads1115.begin(); //Initialize ads1115

  Serial.println("Ajustando la ganancia...");
  ads1115.setGain(GAIN_ONE);

  Serial.println("Tomando medidas del canal AIN0");
  Serial.println("Rango del ADC: +/- 4.096V (1 bit=2mV)");

  // inicializar los objetos de los sensores
  t = Temperatura(ads1115, 2, 0.0348, 0.786, 0.8);
  s = Salinidad(ads1115, 1, 1.6, 2.3);
  h = Humedad(ads1115, 20300, 11800,0);
  i = Iluminacion(ads1115, 3, 1.94);
  w = Wifi(WiFiSSID, WiFiPSK, Server_Host, Rest_Host, MyWriteAPIKey, Server_HttpPort);
  configurarAcelerometro();

  g = new GPS(&gps, GPS_BAUD, INIT_PIN, RX_PIN, TX_PIN);
  g->encender();
  w.conectaWifi();


  Serial.print("Server_Host: ");
  Serial.println(Server_Host);
  Serial.print("Port: ");
  Serial.println(String( Server_HttpPort ));
  Serial.print("Server_Rest: ");
  Serial.println(Rest_Host);

}


//
//  configuramos el pin 4 de interrupcion para que este siempre en 
//  pulso alto y configuramos la interrupcion
//  activamos el wake on motion
//  y finalmente limpiamos la interrupcion por si se ha quedado de antes
//
void configurarAcelerometro(){
  // creamos un objeto acelerometro con el pin de interrupcion 4
  a = Acelerometro(MPU9250_ADDRESS,ACC_FULL_SCALE_16_G,16,4);
  // el pin 4 es el pin donde envia el pulso el acelerometro
  //ponemos el pin 4 que siempre este en puso alto
  pinMode(a.getInterruptPin(), INPUT_PULLUP);
  //ponemo un interrupt en el pin 4 que vaya al metodo handle interrupt enviando un pulso bajo
  attachInterrupt(digitalPinToInterrupt(a.getInterruptPin()), handleInterrupt, CHANGE);
  
  //activamos el wake on motion
  a.enableWakeOnMotion();
  //leemos del registro 3A para evitar problemas si no se ha parado la interrupcion
  a.limpiarInterrupcion();
}
void imprimirResultados(){
  //imprimir resultados
  Serial.println("*****************************************************************");
  Serial.println("*****************************************************************");
  Serial.print("Medida salinidad---------------");
  Serial.print(s.getVolts()); Serial.println(" V, "); //Serial.println(stdev); 
  Serial.print(s.getConductancia()); Serial.println(" micro siemens");
  Serial.print(s.getPorcentaje());Serial.println("%");
  
  Serial.print("Medida humedad---------------");
  Serial.print("HR (%): ");
  Serial.println(h.getPorcentaje());
  
  Serial.print("Medida Temperatura---------------");
  Serial.print(t.getGrados());Serial.println("°C");

  Serial.print("Medida Iluminacion---------------");
  Serial.print(i.getVolts());Serial.println("V");
  Serial.print(i.getPorcentaje());Serial.println("%");

  Serial.println("Medida Acelerometro---------------");
  Serial.print(a.getAX());Serial.println(" G");
  Serial.print(a.getAY());Serial.println(" G");
  Serial.print(a.getAZ());Serial.println(" G");
}

//--------------------------------
//----------LOOP------------------

void loop() {
  // llamar a las funciones medir
  t.medir();
  s.medir();
  h.medir();
  i.medir();
  g->medir();
  a.medir();
  imprimirResultados();
  String data[5];
  data[ 1 ] = t.getGrados();
  data[ 2 ] = h.getPorcentaje();
  data[ 3 ] = s.getPorcentaje();
  data[ 4 ] = i.getPorcentaje();
  data[ 5 ] = "GPS";
  w.httpGet( data );
  
  // esperamos para las suiguientes mesuras
  delay(1000); // 1 segundos
  ESP.deepSleep(10000000);// dormir 10 segundos (tiempo en microsegundos)
  
  
}// ()
