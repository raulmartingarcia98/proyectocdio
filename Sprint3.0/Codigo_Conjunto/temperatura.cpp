// ---------------------------------------------------
//
// temperatura.cpp
//
// ---------------------------------------------------
#include "temperatura.h"
// para evitar error de duplicidad de librerias del ads
#if !defined(FUNCIONES_H)
#define FUNCIONES_H
#include "funciones.h"
#endif
//
// constructor parametrizado
// ads, pin analogico, y los valores de calibracion: la pendiente, el termino independiente y el ajuste que forman la funcion de temperatura grados-voltaje
// 
Temperatura::Temperatura(Adafruit_ADS1115 ads1115v, const int analogicPinV,const double pendienteV,const double terminoIndependienteV,const double ajusteV)
    : ads1115(ads1115v),terminoIndependiente(terminoIndependienteV), ajuste(ajusteV),pendiente(pendienteV),analogicPin(analogicPinV)
{}
// constructor por defecto
Temperatura::Temperatura()
{}

double byteToVoltsGanancia(double value){
  return (value*4.096)/32767;
}

//
//  medir()->Z,Z
//
void Temperatura::medir(){
    int16_t lectura = (*this).ads1115.readADC_SingleEnded((*this).analogicPin);
    (*this).volts = byteToVoltsGanancia(lectura);
    (*this).grados = (((*this).volts - (*this).terminoIndependiente)/(*this).pendiente) + (*this).ajuste;
}
//
//  getPorcentaje() -> Z
//
double Temperatura::getGrados(){return (*this).grados;}
