// ---------------------------------------------------
//
// WIFI.cpp
//
// ---------------------------------------------------

#include "Wifi.h"
#include <ESP8266WiFi.h>


//=========================================================
// Constructores
//=========================================================
Wifi::Wifi() {} //constructor por defecto

// constructor parametrizado
Wifi::Wifi(const String wiFiSSID_V, const String wiFiPSK_V, const String server_Host_V, const String rest_Host_V, const String myWriteAPIKey_V, const int server_HttpPort_V): 
  WiFiSSID(wiFiSSID_V), WiFiPSK(wiFiPSK_V), Server_Host(server_Host_V), Rest_Host(rest_Host_V), MyWriteAPIKey(myWriteAPIKey_V), Server_HttpPort(server_HttpPort_V)
{}


//=========================================================
// funciones
//=========================================================

void Wifi::conectaWifi() {
  byte ledStatus = LOW;

  #ifdef PRINT_DEBUG_MESSAGES
    Serial.print("MAC: ");
    Serial.println(WiFi.macAddress());
  #endif
  
    WiFi.begin(WiFiSSID, WiFiPSK);
  
    while (WiFi.status() != WL_CONNECTED)
    {
      // Blink the LED
      ledStatus = (ledStatus == HIGH) ? LOW : HIGH;
  #ifdef PRINT_DEBUG_MESSAGES
      Serial.println(".");
  #endif
      delay(500);
    }
  #ifdef PRINT_DEBUG_MESSAGES
    Serial.println( "WiFi Connected" );
    Serial.println(WiFi.localIP()); // Print the IP address
  #endif
}

//
//funcion construye el string de datos a enviar a ThingSpeak o Dweet mediante el metodo HTTP GET
// La funcion envia "numFields" datos, del array fieldData.
// Asegurate de ajustar "numFields" al número adecuado de datos que necesitas enviar y activa los campos en tu canal web
//
void Wifi::httpGet(const String fieldData[]) {

  if (client.connect( Server_Host , Server_HttpPort )) {

    // empezamos a crear la ruta para el get
    String PostData = "GET http://dweet.io/dweet/for/";
    PostData = PostData + MyWriteAPIKey + "?" ;

    // le añadimos los datos que vamos a enviar
    PostData += "&Temp";
    PostData += "=" + String(fieldData[ 1 ]);
    PostData += "&Hume" ;
    PostData += "=" + String(fieldData[ 2 ]);
    PostData += "&Salin" ;
    PostData += "=" + String(fieldData[ 3 ]);
    PostData += "&Ilumi" ;
    PostData += "=" + String(fieldData[ 4 ]);
    PostData += "&GPS" ;
    PostData += "=" + String(fieldData[ 5 ]);


#ifdef PRINT_DEBUG_MESSAGES
    Serial.println( "Datos enviados a:" );
#endif
    client.print(PostData);
    client.println(" HTTP/1.1");
    client.println("Host: " + String(Rest_Host));
    client.println("Connection: close");
    client.println();
#ifdef PRINT_DEBUG_MESSAGES
    Serial.println( PostData );
    Serial.println();
    //Para ver la respuesta del servidor
#ifdef PRINT_HTTP_RESPONSE
    delay(500);
    Serial.println();
    while (client.available()) {
      String line = client.readStringUntil('\r');
      Serial.print(line);
    }
    Serial.println();
    Serial.println();
#endif
#endif
  }
}
