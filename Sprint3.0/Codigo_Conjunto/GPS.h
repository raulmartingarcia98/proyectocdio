//-------------
//
//  GPS.h
//  clase para el gps
//
//--------------

#ifndef GPS_H
#define GPS_H
#include "Arduino.h"
#include <SoftwareSerial.h>
#include "TinyGPS++.h"
// para evitar error de duplicidad de librerias del ads
//#if !defined(TINYGPS)
//#define TINYGPS
//#endif

class GPS {

  private:
    int gpsBaud; //  velocidad de comunicación serie 
    int initPin;// pin para inicializar el gps
    int rxPin;// GPS RXI
    int txPin;// GPS TX0
    TinyGPSPlus * gps;

    //posicion del gps
    double lat;
    double lng;
    double satelites;
    double altitudeGps;
    // tiempo en la que ha hecho la lectura
    double hour;
    double minute;
    double seconds;
    // fecha y tiempo en texto
    char * tiempo;
    char * fecha;

    SoftwareSerial ss;// Creamos una UART software para comunicación con el GPS
    void smartDelay(unsigned long);// Función espera 1s para leer del GPS
    void switch_on_off();// Función para encender/apagar mediante un pulso

  public:
    GPS();
    GPS(TinyGPSPlus*, const int, const int, const int,const int/*, SoftwareSerial * ss*/);
    void medir();
    void encender();
    // getters
    double getLat();
    double getLng();
    double getAlt();
    int getSatelites();
    double getTimeSeconds();
    char getTime();
    char getDate();
};
#endif
