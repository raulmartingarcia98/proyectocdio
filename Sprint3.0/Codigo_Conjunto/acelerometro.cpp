//===================================
//
// Sensor acelerometro.cpp
//
//=====================================

#include "Acelerometro.h"
#include <Wire.h>

Acelerometro::Acelerometro() {}

Acelerometro::Acelerometro(const uint8_t mpuAdressV,const uint8_t accFullScaleV, 
                          const uint8_t fsAccV,const int interruptPinV): accFullScale(accFullScaleV), 
                          accMPUAdress(mpuAdressV), fsAcc(fsAccV),interruptPin(interruptPinV)
{

  // inicializamos el sensor con los parametros
  // indicando el addres del sensor,registro del fondo de escala y el valor fondo de escala
  I2CwriteByte(mpuAdressV, 28, accFullScaleV);

  
  

}


//Funcion auxiliar lectura
void Acelerometro::I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* Data)
{
  // transmision al address indicado
  Wire.beginTransmission(Address);
  // escribir registro a leeer
  Wire.write(Register);
  Wire.endTransmission();

  // leer valores
  Wire.requestFrom(Address, Nbytes);
  uint8_t index = 0;
  while (Wire.available())
    Data[index++] = Wire.read();
}


// Funcion auxiliar de escritura
void Acelerometro::I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data)
{
  // transmision al address indicado
  Wire.beginTransmission(Address);
  // escribir registro a leeer
  Wire.write(Register);
  // escribir datos en el registro
  Wire.write(Data);
  Wire.endTransmission();
}

void Acelerometro::medir() {
  // ---  Lectura acelerometro y giroscopio ---
  uint8_t Buf[6];

  // leemos del sensor y llenamos el array buf
  I2Cread((*this).accMPUAdress, 0x3B, 6, Buf);

  //Convertir registros acelerometro
  float axV = (Buf[0] << 8 | Buf[1]);
  float ayV = (Buf[2] << 8 | Buf[3]);
  float azV = Buf[4] << 8 | Buf[5];

  //Corregir inexactitud medida eje z (en bits)
  //Calculado a partir de la lectura en bits del acelerómetro en reposo (1.14g-1g);
  float coef_calib_z = -300;


  //Valores del acelerómetro en g's. Si el valor del eje es mayor que 2^15, el valor en g's será negativo
  if (axV > 32768) {
    axV = axV - (32768 * 2);
    axV = axV * (*this).fsAcc / 32768;
  } else {
    axV = axV * (*this).fsAcc / 32768;
  }
  if (ayV > 32768) {
    ayV = ayV - (32768 * 2);
    ayV = ayV * (*this).fsAcc / 32768;
  } else {
    ayV = ayV * (*this).fsAcc / 32768;
  }
  if (azV > 32768) {
    azV = azV - (32768 * 2) + coef_calib_z;
    azV = azV * (*this).fsAcc / 32768;
  } else {
    azV = (azV + coef_calib_z) * (*this).fsAcc / 32768;
  }

  (*this).ax = axV;
  (*this).ay = ayV;
  (*this).az = azV;
  
}

//funcion que se usa par detener el pulso alto de la interrupcion
// cuando se esta configurado de que no se detenga hasta que lea del registro 0x3A
void Acelerometro::limpiarInterrupcion(){
  uint8_t intStatus[1];
  //configurado para que la interrupcion termine cuando lees del registro LATCH_INT_EN
  I2Cread((*this).accMPUAdress,0x3A,1,intStatus);
}

//Funcion wake-on-motion
void Acelerometro::enableWakeOnMotion(){

  // Clear registers
  I2CwriteByte((*this).accMPUAdress, 0x6B, 0x00);
   // Enable accelerometer, disable gyro
  I2CwriteByte((*this).accMPUAdress, 0x6C, 0x07);
  // Set Accel LPF setting to 184 Hz Bandwidth
  I2CwriteByte((*this).accMPUAdress, 0x1D, 0x09);
   // Enable Motion Interrupt
  I2CwriteByte((*this).accMPUAdress, 0x38, 0x40);
  // Enable Accel Hardware Intelligence
  I2CwriteByte((*this).accMPUAdress, 0x69, 0xC0);
  // Set Motion Threshold: calibrar la sensibilidad de movimiento para despertar
  I2CwriteByte((*this).accMPUAdress, 0x1F, 0xF0);
  // Set Frequency of Wake-up: filtro paso bajo
  I2CwriteByte((*this).accMPUAdress, 0x1E, 0x05);
  // Enable Cycle Mode (Accel Low Power Mode)
  I2CwriteByte((*this).accMPUAdress, 0x6B, 0x20);
  // Configurar el registro de interrupcion para que pare el pulso 
  // cuando leas del registro 0x3A
  I2CwriteByte((*this).accMPUAdress, 0x37, 128);


  limpiarInterrupcion();
  
}

// getters
double Acelerometro::getAX(){
  return (*this).ax;
}
double Acelerometro::getAY(){
  return (*this).ay;
}
double Acelerometro::getAZ(){
  return (*this).az;
}
int Acelerometro::getInterruptPin(){
  return (*this).interruptPin;
}
