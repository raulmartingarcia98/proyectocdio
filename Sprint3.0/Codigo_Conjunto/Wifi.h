//-------------
//
//  wifi.h
//  clase para la conexion wifi
//
//--------------
#ifndef WIFI_YA_INCLUID
#define WIFI_YA_INCLUID
#define PRINT_DEBUG_MESSAGES
#include <ESP8266WiFi.h>

class Wifi{

  private: 
    String WiFiSSID;
    String WiFiPSK;
    String Server_Host;
    String Rest_Host;

    String MyWriteAPIKey;
    
    int Server_HttpPort;

    WiFiClient client;

    public:
      Wifi();
      Wifi(const String ,const String ,const String ,const String ,const String, const int);

      void conectaWifi();
      void httpGet(const String[]);
    

  
};


#endif
