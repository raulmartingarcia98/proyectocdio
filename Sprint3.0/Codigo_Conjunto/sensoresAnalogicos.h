//-------------
//
//  salinidad.h temperatura.h humedad.h
//  todas las clases de los sensores analogicos
//
//--------------


#include <Adafruit_ADS1015.h>
#include "funciones.h"


class Salinidad{

    private:
        int powerPin;
        int analogicPin;
        
        int valorCienPorCiento;
        int valorCeroPorCiento;
        
        double porcentaje;
        double conductancia;
        double volts;
        
        Adafruit_ADS1115 ads1115;
        
        void salinity_reading_stats( int , float *, float * );
        double getConductanciaUS(const double);
        double porcentajeSalinidad(const double);

    public:
        Salinidad(Adafruit_ADS1115, const int, const int , const double, const double);
        void medir();
        double getPorcentaje();
        double getConductancia();
        double getVolts();
    
};
// ---------------------------------------------------
//
// Sensor.cpp
//
// ---------------------------------------------------
//#include "sensoresAnalogicos.h"


Salinidad::Salinidad(Adafruit_ADS1115 ads1115v, const int powerPinV, const int analogicPinV ,const double valor0v,const double valor100v)
    : ads1115(ads1115v),valorCeroPorCiento(valor0v), valorCienPorCiento(valor100v),
    powerPin(powerPinV),analogicPin(analogicPinV)
{}


void Salinidad::medir(){
  //int salinity_input_pin = 1; //Pin A1 Salinidad
  int nave = 20; // 20 lecturas
  float ave, stdev;
  salinity_reading_stats(nave, &ave, &stdev);
  // pasamos el resultado a volts
  (*this).volts = byteToVoltsGanancia1(ave);
  (*this).conductancia = getConductanciaUS((*this).volts);
  (*this).porcentaje = porcentajeSalinidad((*this).volts);
  
  
}
double Salinidad::porcentajeSalinidad(double voltajeAnalogico){ //Medida salinidad en función del voltaje
  double intervaloVoltios = 1;  //Intérvalo existente entra el valor 100(2.3v) y 0(1.6v)
  if (voltajeAnalogico <= (*this).valorCeroPorCiento){
    return 0.0;
  }else if (voltajeAnalogico >= (*this).valorCienPorCiento){
    return 100.0;
  }else{
    return (voltajeAnalogico-1.6)/0.007; 
  }
  
}// ()
double Salinidad::getConductanciaUS(double voltajeAnalogico){
  // calculamos la resistencia aillada de la formula del divisor de tension aplicado en la resistencia
  // de 10 k ohmios
  double resistencia = (3.33*10000-voltajeAnalogico*10000)/voltajeAnalogico;
  double conductancia = 1/resistencia;
  return conductancia*1000000;
}//()

///------------------------------
/// Calcula la media y la desviacion estandard de las medidad
/// N,N,N -> salinity_reading_stats() -> R,R
///------------------------------
void Salinidad::salinity_reading_stats( int nave, float *ave, float *stdev ) {
 int buffer_length = 100; // Size of array to store readings for computation of ave and stdev
 // Reduce BUFFER_LENGTH to save memory if statistics are OK
 // with smaller sample size
 int i, n;
 float sum;
 float reading[buffer_length]; // Array para guardar las lecturas

 n = min( nave, buffer_length ); // Make sure we don't over-run the data buffer 
 
 // -- Guardar lecturas en el array
 for ( i=0; i<n; i++ ) {
  
   digitalWrite( (*this).powerPin, HIGH ); // Supply power to the sensor
   delay(100); // Wait for sensor to settle
   int16_t adc0 = ads1115.readADC_SingleEnded((*this).analogicPin);// el canal donde se va a leer (A0...A3)
   reading[i] = (float) (adc0*100)/100;
   digitalWrite( (*this).powerPin, LOW ); // Turn off power to the sensor
   delay(10); // esperar entre lecturas
 }// for
 
 // -- Calcular la media y la desviacion tipica.
 for ( sum=0.0, i=0; i<n; i++ ) {
    sum += reading[i];
 }// for
 
 *ave = sum/float(nave);

 for ( sum=0.0, i=0; i<n; i++ ) {
   sum += pow(reading[i] - *ave, 2);
 }// for
 
 *stdev = sqrt( sum/float(n-1) );
 
}// ()


double Salinidad::getPorcentaje(){return (*this).porcentaje;}
double Salinidad::getConductancia(){return (*this).conductancia;}
double Salinidad::getVolts(){return (*this).volts;}


class Humedad{

    private:
        int valorCienPorCiento;
        int valorCeroPorCiento;
        double porcentaje;
        Adafruit_ADS1115 ads1115;

    public:
        Humedad(Adafruit_ADS1115, const int, const int);
        void medir();
        double getPorcentaje();
    
};

// ---------------------------------------------------
//
// humedad.cpp
//
// ---------------------------------------------------
//#include "sensoresAnalogicos.h"

//
// constructor
//
Humedad::Humedad(Adafruit_ADS1115 ads1115v, const int valor0v,const int valor100v)
    : ads1115(ads1115v),valorCeroPorCiento(valor0v), valorCienPorCiento(valor100v)
{}

//
//  medir()->Z,Z
//
void Humedad::medir(){
    //guardamos la lectura
    int16_t lecturaAnalogica = (*this).ads1115.readADC_SingleEnded(0); 
    // calculamos el porcentaje de humedad
    (*this).porcentaje = 100 * (*this).valorCienPorCiento / ((*this).valorCienPorCiento - (*this).valorCeroPorCiento)
             - lecturaAnalogica * 100 / ((*this).valorCienPorCiento - (*this).valorCeroPorCiento);

    // comprobamos si da menos que 0 o mas que 100 para no dar valores erroneos
    if ((*this).porcentaje <= 0) {
      (*this).porcentaje= 0;
    } else if ((*this).porcentaje >= 100) {
      (*this).porcentaje = 100;
    }
}
//
//  getPorcentaje() -> Z
//
double Humedad::getPorcentaje(){return (*this).porcentaje;}
class Temperatura{

    private:

        int analogicPin;
        double grados;
        double pendiente;
        double terminoIndependiente;
        double ajuste;
        Adafruit_ADS1115 ads1115;

    public:
        Temperatura(Adafruit_ADS1115,const int, const double, const double,const double);
        void medir();
        double getGrados();
    
};

// ---------------------------------------------------
//
// temperatura.cpp
//
// ---------------------------------------------------
//#include "sensoresAnalogicos.h"
//
// constructor
//
Temperatura::Temperatura(Adafruit_ADS1115 ads1115v, const int analogicPinV,const double pendienteV,const double terminoIndependienteV,const double ajusteV)
    : ads1115(ads1115v),terminoIndependiente(terminoIndependienteV), ajuste(ajusteV),pendiente(pendienteV),analogicPin(analogicPinV)
{}

//
//  medir()->Z,Z
//
void Temperatura::medir(){
    int16_t lectura = (*this).ads1115.readADC_SingleEnded((*this).analogicPin);
    Serial.println(lectura);
    double volts = byteToVoltsGanancia1(lectura);
    Serial.println(volts);
    (*this).grados = ((volts - (*this).terminoIndependiente)/(*this).pendiente) + (*this).ajuste;
}
//
//  getPorcentaje() -> Z
//
double Temperatura::getGrados(){return (*this).grados;}
