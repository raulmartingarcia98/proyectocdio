
//-------------
//
//  humedad.h 
//  clase para el sensor de humedad
//
//--------------
#ifndef ACELEROMETRO_H
#define ACELEROMETRO_H
#include <Wire.h>
 #if defined(ARDUINO) && ARDUINO >= 100
      #include "Arduino.h"
    #else
      #include "WProgram.h"
    #endif

class Acelerometro{

    private:
        // valores de ajuste en hexadecimal
        uint8_t accFullScale; // fondo de escala de mesura
        uint8_t accMPUAdress; // direccion del sensor

        int interruptPin;
        int fsAcc;//factor de conversion de bytes a G's
        
        // valores de ejes de aceleracion
        double ax;
        double ay;
        double az;
        //funciones privadas
        // leer datos de aceleracion del sensor
        void I2Cread(uint8_t , uint8_t , uint8_t , uint8_t*);
        // escribir registros al sensor
        void I2CwriteByte(uint8_t , uint8_t , uint8_t );
       

    public:
        //constructores
        Acelerometro();
        Acelerometro(const uint8_t, const uint8_t, const uint8_t,const int);

        // funciones
        void medir();
        void enableWakeOnMotion();// WAKE ON MOTION
        void limpiarInterrupcion();
        
        // getters
        double getAX();
        double getAY();
        double getAZ();
        int getInterruptPin();
    
};

#endif
