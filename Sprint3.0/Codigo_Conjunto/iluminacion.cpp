// ---------------------------------------------------
//
// temperatura.cpp
//
// ---------------------------------------------------
#include "iluminacion.h"
// para evitar error de duplicidad de librerias del ads
#if !defined(FUNCIONES_H)
#define FUNCIONES_H
#endif
//
// constructor parametrizado
// ads, pin analogico, y los valores de calibracion: la pendiente, el termino independiente y el ajuste que forman la funcion de temperatura grados-voltaje
//
Iluminacion::Iluminacion(Adafruit_ADS1115 ads1115v, const int analogicPinV, const double saturacionV)
  : ads1115(ads1115v), saturacion(saturacionV), analogicPin(analogicPinV)
{}
// constructor por defecto
Iluminacion::Iluminacion()
{}

//
//  medir()->Z
//
void Iluminacion::medir() {
  int lectura = (*this).ads1115.readADC_SingleEnded((*this).analogicPin);
  (*this).volts = (lectura * 4.096) / 32767;
}
//
//  getPorcentaje() -> Z
//
double Iluminacion::getVolts() {
  return (*this).volts;
}

double Iluminacion::getPorcentaje() {

if (0.08 >= (*this).volts){
    return 0.0;
  }else if ((*this).saturacion <= (*this).volts){
    return 100.0;
  }else{
    return ((*this).volts*100)/(*this).saturacion; 
  }
  
}
