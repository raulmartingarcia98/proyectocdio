// ---------------------------------------------------
//
// GPS.cpp
//
// ---------------------------------------------------
#include "GPS.h"
#include "TinyGPS++.h"  //Librería del GPS

//
// constructor parametrizado
// ads, pin analogico, y los valores de calibracion: la pendiente, el termino independiente y el ajuste que forman la funcion de temperatura grados-voltaje
//
GPS::GPS(TinyGPSPlus * gpsV,const int gpsBaudV,const int initPinV,const int rxPinV,const int txPinV/*, SoftwareSerial * serial*/)
  : gps(gpsV),gpsBaud(gpsBaudV),initPin(initPinV),rxPin(rxPinV),txPin(txPinV),ss(rxPinV, txPinV)
{
 // ss = serial;
}
// constructor por defecto
GPS::GPS():ss(0,0)
{
  //(*this).ss = SoftwareSerial(0,0);
}

//
//  medir()->Z
//
void GPS::medir() {


  (*this).fecha[10];
  (*this).tiempo[10];

  if ((*this).gps->location.isValid()) { // Si el GPS está recibiendo los mensajes NMEA

    // tiempo en forma de texto para visualizarlo
    sprintf((*this).fecha, "%d/%d/%d", (*this).gps->date.month(), (*this).gps->date.day(), (*this).gps->date.year()); // Construimos string de datos fecha
    sprintf((*this).tiempo, "%d/%d/0%d", (*this).gps->time.hour(), (*this).gps->time.minute(), (*this).gps->time.second()); // Construimos string de datos hora

    // coordenadas
    (*this).lat = (*this).gps->location.lat();
    (*this).lng = (*this).gps->location.lng();
    (*this).altitudeGps = (*this).gps->altitude.feet();
  
    //tiempo en valores para ralizar calculos
    (*this).hour = (*this).gps->time.hour();
    (*this).minute = (*this).gps->time.minute();
    (*this).seconds = (*this).gps->time.second();
  }
  else{ // Si no recibe los mensajes
   // satelites
    (*this).satelites = (*this).gps->satellites.value();
  }
  
  smartDelay(1000);
}

// Función espera1s para leer del GPS
void GPS::smartDelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while(ss.available())
    {
      (*this).gps->encode(ss.read());  // leemos del gps
    }
  } while(millis() - start < ms);
}
// Función para encender/apagar mediante un pulso
void GPS::switch_on_off()
{
   // Power on pulse
  digitalWrite((*this).initPin,LOW);
  delay(200);
  digitalWrite((*this).initPin,HIGH);
  delay(200); 
  digitalWrite((*this).initPin,LOW);
 }

 // Función pública para inicializar el gps
void GPS::encender(){
  
  ss.begin((*this).gpsBaud);
  pinMode((*this).initPin,OUTPUT); 
  switch_on_off();
}

// getters
double GPS::getLat(){
  return (*this).lat;
}

double GPS::getLng(){
  return (*this).lng;
}

double GPS::getAlt(){
  return (*this).altitudeGps;
}

int GPS::getSatelites(){
  return (*this).satelites;
}
//coge la hora los minutos y los segundos y lo devolvemos todo en segundos
double GPS::getTimeSeconds(){
  double res = 0;
  res+= (*this).hour*60*60; // de horas a segundos
  res+= (*this).minute*60; // de minutos a segundos
  res+= (*this).seconds;
  
}

char GPS::getTime(){
  return  *(*this).tiempo;
}
char  GPS::getDate(){
  return  *(*this).fecha;
}
