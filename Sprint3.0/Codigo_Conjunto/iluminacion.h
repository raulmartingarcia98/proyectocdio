//-------------
//
//  temeperatura.h 
//  clase para el sensor de temperatura
//
//--------------

#ifndef ILUMINACION_H
#define ILUMINACION_H
// para evitar error de duplicidad de librerias del ads
#if !defined(ADS)
#define ADS
#include <Adafruit_ADS1015.h>
#endif

class Iluminacion{

    private:

        int analogicPin;
        double saturacion;
        Adafruit_ADS1115 ads1115;
        double volts;

    public:
        Iluminacion();
        Iluminacion(Adafruit_ADS1115,const int, const double);
        void medir();
        double getVolts();
        double getPorcentaje();
    
};
#endif
