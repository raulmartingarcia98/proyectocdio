//GND - GND
//VCC - VCC
//SDA - Pin 2
//SCL - Pin 14
 
#include <Wire.h>
 
#define    MPU9250_ADDRESS            0x68

#define    ACC_FULL_SCALE_2_G        0x00  //Configurar selección d'escala acceleròmetre en els bits 3 i 4 (00)
#define    ACC_FULL_SCALE_4_G        0x08  // (01)
#define    ACC_FULL_SCALE_8_G        0x10  // (10)
#define    ACC_FULL_SCALE_16_G       0x18  // (11)


//Funcion auxiliar lectura
void I2Cread(uint8_t Address, uint8_t Register, uint8_t Nbytes, uint8_t* Data)
{
   Wire.beginTransmission(Address);
   Wire.write(Register);
   Wire.endTransmission();
 
   Wire.requestFrom(Address, Nbytes);
   uint8_t index = 0;
   while (Wire.available())
      Data[index++] = Wire.read();
}
 
 
// Funcion auxiliar de escritura
void I2CwriteByte(uint8_t Address, uint8_t Register, uint8_t Data)
{
   Wire.beginTransmission(Address);
   Wire.write(Register);
   Wire.write(Data);
   Wire.endTransmission();
}

//Funcion wake-on-motion
bool womEnable(){
  uint8_t val;
  uint8_t Register;
  // Clear registers
  val = 0x80;
  Register = 0x6B;
  I2CwriteByte(MPU9250_ADDRESS, Register, val);
  delay(10);

  // Enable accelerometer, disable gyro
  val = 0x07;
  Register = 0x6C;
  I2CwriteByte(MPU9250_ADDRESS, Register, val);
  delay(10);

  // Set Accel LPF setting to 184 Hz Bandwidth
  val = 0x0F;
  Register = 0x1D;
  I2CwriteByte(MPU9250_ADDRESS, Register, val);
  delay(10);

  // Enable Motion Interrupt
  val = 0x40;
  Register = 0x38;
  I2CwriteByte(MPU9250_ADDRESS, Register, val);
  delay(10);

  // Enable Accel Hardware Intelligence
  val = 0xC0;
  Register = 0x69;
  I2CwriteByte(MPU9250_ADDRESS, Register, val);
  delay(10);

  // Set Motion Threshold: calibrar la sensibilidad de movimiento para despertar
  val = 0x50;
  Register = 0x1F;
  I2CwriteByte(MPU9250_ADDRESS, Register, val);
  delay(10);

  // Set Frequency of Wake-up: filtro paso bajo
  val = 0x07;
  Register = 0x1E;
  I2CwriteByte(MPU9250_ADDRESS, Register, val);
  delay(10);


  // Enable Cycle Mode (Accel Low Power Mode)
  val = 0x20;
  Register = 0x6B;
  I2CwriteByte(MPU9250_ADDRESS, Register, val);
  delay(10);

  return true;
}


void setup()
{
  Wire.begin();
  Serial.begin(9600);
  Serial.println("Inicializando...");
  Serial.println("Configurando acelerónetro...");
   // Configurar acelerometro
  I2CwriteByte(MPU9250_ADDRESS, 28, ACC_FULL_SCALE_16_G);
 // womEnable();
}
 
 
void loop()
{
   // ---  Lectura acelerometro y giroscopio --- 
   uint8_t Buf[6];
   int FS_ACC = 16;

   I2Cread(MPU9250_ADDRESS, 0x3B, 6, Buf);
 
//Convertir registros acelerometro
   float ax = (Buf[0] << 8 | Buf[1]);
   float ay = (Buf[2] << 8 | Buf[3]);
   float az = Buf[4] << 8 | Buf[5];

//Corregir inexactitud medida eje z (en bits)
//Calculado a partir de la lectura en bits del acelerómetro en reposo (1.14g-1g);
  float coef_calib_z = -300;

  
//Valores del acelerómetro en g's. Si el valor del eje es mayor que 2^15, el valor en g's será negativo   
   if (ax > 32768){
    ax = ax-(32768*2);
    ax = ax*FS_ACC/32768;
   }else{
    ax = ax*FS_ACC/32768;
   }
   if (ay > 32768){
    ay = ay-(32768*2);
    ay = ay*FS_ACC/32768;
   }else{
    ay = ay*FS_ACC/32768;
   }
   if (az > 32768){
    az = az-(32768*2)+coef_calib_z;
    az = az*FS_ACC/32768;
   }else{
    az = (az+coef_calib_z)*FS_ACC/32768;
   }
   
 
   // --- Mostrar valores ---
   Serial.println("Lectura Acelerometro");
   Serial.print("AX=");
   Serial.print(ax, 2);
   Serial.print("g");
   Serial.print("\t");
   Serial.print("AY=");
   Serial.print(ay, 2);
   Serial.print("g");
   Serial.print("\t");
   Serial.print("AZ=");
   Serial.print(az, 2);
   Serial.println("g");

 
// Fin medicion
  Serial.println("");
  
  ESP.deepSleep(10000000); //tiempo de 10 segundos(en microsegundos)
      
}
