
#include <Wire.h>
#include <iostream>
using namespace std;


//--------------------------------
//----------CONSTANTES------------
#include <Adafruit_ADS1015.h>
Adafruit_ADS1115 ads1115(0x48); // construct an ads1115 at address 0x48
#define POWER_PIN 5 // Digital I/O pin

void setup() {

  //Setup del sensor de humedad
  Serial.begin(9600);
  Serial.println("Inicializando...");
   ads1115.begin(); //Initialize ads1115
  Serial.println("Ajustando la ganancia...");
  ads1115.setGain(GAIN_ONE);

  Serial.println("Tomando medidas del canal AIN0");
  Serial.println("Rango del ADC: +/- 4.096V (1 bit=2mV)");

  //pinMode(POWER_PIN, OUTPUT);//configurar salida al pin de salinidad*/

}

double byteToVolts(int value){
  return (value*4.096)/32767;
}

void loop() {
  // put your main code here, to run repeatedly:

  int16_t lectura = ads1115.readADC_SingleEnded(2);
  double voltios = byteToVolts(lectura);
  double grados = ((voltios - 0.786)/0.0348) + 0.8;


  Serial.println("-----------------");
  Serial.print("Lectura: ");
  Serial.println(lectura);
  Serial.print("Voltios: ");
  Serial.println(voltios);


  if(grados > 40){
    Serial.println("PELIGRO temperatura Mayor que 40");
  }else if(grados < 0){
    Serial.println("PELIGRO temperatura Menor que 00");
  }else{
    Serial.print("Grados: ");
    Serial.println(grados);
  }
  delay(2000);
  
}
