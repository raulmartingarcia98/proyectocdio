int salinity_power_pin = 5; // Digital I/O pin
#define BUFFER_LENGTH 100 // Size of array to store readings for computation of ave and stdev
 // Reduce BUFFER_LENGTH to save memory if statistics are OK
 // with smaller sample size
 
#include <Adafruit_ADS1015.h>
Adafruit_ADS1115 ads1115(0x48); // construct an ads1115 at address 0x48

 
void setup()
{
  Serial.begin(9600);
  Serial.println("Inicializando...");

  // ADS, ganancia
  ads1115.begin(); //Initialize ads1115
  Serial.println("Ajustando la ganancia...");
  ads1115.setGain(GAIN_ONE);
  Serial.println("Tomando medidas del canal AIN0");
  pinMode(salinity_power_pin, OUTPUT);

 
}
void loop()
{
 int salinity_input_pin = 1; //Pin A1 Salinidad
 int nave = 20;
 float ave, stdev;
 salinity_reading_stats( salinity_power_pin, salinity_input_pin, nave, &ave, &stdev);
 double volts = byteToVolts(ave);
 //Serial.print(ave);Serial.print(" bits, ");
 Serial.print(volts); Serial.println(" V, "); //Serial.println(stdev); 
 Serial.print(getConductanciaUS(volts)); Serial.println(" micro siemens");
 Serial.print(porcentajeSalinidad(volts));Serial.println("%");
}

double byteToVolts(int value){
  return (value*4.096)/32767;
}

double getConductanciaUS(double voltajeAnalogico){
  double resistencia = (3.33*10000-voltajeAnalogico*10000)/voltajeAnalogico;
  Serial.print(resistencia); Serial.println(" Ohmios");
  double conductancia = 1/resistencia;
  return conductancia*1000000;
}

double porcentajeSalinidad(double voltajeAnalogico){ //Medida salinidad en función del voltaje
  double intervaloVoltios = 1;  //Intérvalo existente entra el valor 100(2.3v) y 0(1.6v)
  if (voltajeAnalogico <= 1.6){
    return 0.0;
  }else if (voltajeAnalogico >= 2.3){
    return 100.0;
  }else{
    return (voltajeAnalogico-1.6)/0.007; 
  }
  
}

///------------------------------
/// Calcula la media y la desviacion estandard de las medidad
///------------------------------
void salinity_reading_stats( int power_pin, int input_pin, int nave, float *ave, float *stdev ) {

 int i, n;
 float sum; // Use a float to prevent overflow
 float reading[BUFFER_LENGTH]; // Array to store readings

 n = min( nave, BUFFER_LENGTH ); // Make sure we don't over-run the data buffer
 // -- Store readings in an array
 for ( i=0; i<n; i++ ) { // First array index is 0, last is n-1
 digitalWrite( power_pin, HIGH ); // Supply power to the sensor
 delay(100); // Wait for sensor to settle
 int16_t adc0 = ads1115.readADC_SingleEnded(input_pin);// el canal donde se va a leer (A0...A3)
 reading[i] = (float) (adc0*100)/100;
 //reading[i] = analogRead( input_pin ); // Add reading to the running sum
 digitalWrite( power_pin, LOW ); // Turn off power to the sensor
 delay(10); // wait between readings
 }
 // -- Compute average and standard deviation.
 for ( sum=0.0, i=0; i<n; i++ ) {
 sum += reading[i];
 }
 *ave = sum/float(nave);

 for ( sum=0.0, i=0; i<n; i++ ) {
 sum += pow(reading[i] - *ave, 2);
 }
 *stdev = sqrt( sum/float(n-1) );
}
