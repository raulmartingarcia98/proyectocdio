 
//Autor: Grupo 11.
//Fecha: 22/10/2019.
//Descripción: Codigo completo del Arduino.

//--------------------------------
//----------LIBRERIAS-------------

#include <Wire.h>
#include <Adafruit_ADS1015.h>

//--------------------------------
//----------CONSTANTES------------

Adafruit_ADS1115 ads1115(0x48); // construct an ads1115 at address 0x48
#define SALINITY_POWER_PIN 5 // Digital I/O pin


//--------------------------------
//----------SETUP-----------------

void setup() {

  //Setup del sensor de humedad
  Serial.begin(9600);
  Serial.println("Inicializando...");
  ads1115.begin(); //Initialize ads1115
  Serial.println("Ajustando la ganancia...");
  ads1115.setGain(GAIN_ONE);

  Serial.println("Tomando medidas del canal AIN0");
  Serial.println("Rango del ADC: +/- 4.096V (1 bit=2mV)");

  pinMode(SALINITY_POWER_PIN, OUTPUT);//configurar salida al pin de salinidad

}
//
// pasa de byte a volts sabiendo que la ganancia del ads esta
// configurada a uno, por lo tanto el maximo son 4.096 volts
// Se aplica una regla de 3
// Z->byteToVolts()->R
//
double byteToVolts(int value){
  return (value*4.096)/32767;
}

//
// calcula la conductancia del agua mediante la formula del divisor de tension
//  R-> getConductanciaUS() ->R
// 
double getConductanciaUS(double voltajeAnalogico){
  // calculamos la resistencia aillada de la formula del divisor de tension aplicado en la resistencia
  // de 10 k ohmios
  double resistencia = (3.33*10000-voltajeAnalogico*10000)/voltajeAnalogico;
  double conductancia = 1/resistencia;
  return conductancia*1000000;
}//()

//
// calcula el porcentaje de salinidad respecto al voltaje
// R -> porecentajeSalinidad() -> R
//
double porcentajeSalinidad(double voltajeAnalogico){ //Medida salinidad en función del voltaje
  double intervaloVoltios = 1;  //Intérvalo existente entra el valor 100(2.3v) y 0(1.6v)
  if (voltajeAnalogico <= 1.6){
    return 0.0;
  }else if (voltajeAnalogico >= 2.3){
    return 100.0;
  }else{
    return (voltajeAnalogico-1.6)/0.007; 
  }
  
}// ()

///------------------------------
/// Calcula la media y la desviacion estandard de las medidad
/// N,N,N -> salinity_reading_stats() -> R,R
///------------------------------
void salinity_reading_stats( int power_pin, int input_pin, int nave, float *ave, float *stdev ) {
 int buffer_length = 100; // Size of array to store readings for computation of ave and stdev
 // Reduce BUFFER_LENGTH to save memory if statistics are OK
 // with smaller sample size
 int i, n;
 float sum;
 float reading[buffer_length]; // Array para guardar las lecturas

 n = min( nave, buffer_length ); // Make sure we don't over-run the data buffer 
 
 // -- Guardar lecturas en el array
 for ( i=0; i<n; i++ ) {
  
   digitalWrite( power_pin, HIGH ); // Supply power to the sensor
   delay(100); // Wait for sensor to settle
   int16_t adc0 = ads1115.readADC_SingleEnded(input_pin);// el canal donde se va a leer (A0...A3)
   reading[i] = (float) (adc0*100)/100;
   digitalWrite( power_pin, LOW ); // Turn off power to the sensor
   delay(10); // esperar entre lecturas
 }// for
 
 // -- Calcular la media y la desviacion tipica.
 for ( sum=0.0, i=0; i<n; i++ ) {
    sum += reading[i];
 }// for
 
 *ave = sum/float(nave);

 for ( sum=0.0, i=0; i<n; i++ ) {
   sum += pow(reading[i] - *ave, 2);
 }// for
 
 *stdev = sqrt( sum/float(n-1) );
 
}// ()

//----------------------------------------------
// Logica de cada loop de la mesura de salinidad 
// mesuraSalinidad -> R,R,R
//----------------------------------------------
void mesuraSalinidad(double *volts, double *conductancia,double*porcentaje){
  int salinity_input_pin = 1; //Pin A1 Salinidad
  int nave = 20; // 20 lecturas
  float ave, stdev;
  salinity_reading_stats( SALINITY_POWER_PIN, salinity_input_pin, nave, &ave, &stdev);
  // pasamos el resultado a volts
  *volts = byteToVolts(ave);
  *conductancia = getConductanciaUS(*volts);
  *porcentaje = porcentajeSalinidad(*volts);
}

//----------------------------------------------
// Logica de cada loop de la mesura de humedad 
// mesuraHumedad -> N,N
//----------------------------------------------
void mesuraHumedad(int16_t *lectura, int16_t * porcentajeHumedad){
  
  int AirValue = 20300;  // valor en seco en bytes
  int WaterValue = 11800;  // valor en agua en bytes
  //guardamos la lectura
  *lectura = ads1115.readADC_SingleEnded(0);
  // calculamos el porcentaje de humedad
  *porcentajeHumedad = 100 * AirValue / (AirValue - WaterValue) - *lectura * 100 / (AirValue - WaterValue);

  // comprobamos si da menos que 0 o mas que 100 para no dar valores erroneos
  if (*porcentajeHumedad <= 0) {
    *porcentajeHumedad = 0;
  } else if (*porcentajeHumedad >= 100) {
    *porcentajeHumedad = 100;
  }
}

//--------------------------------
//----------LOOP------------------

void loop() {

  double voltsSalinidad;
  double conductanciaSalinidad;
  double porcentajeSalinidad;

  int16_t lecturaHumedad;
  int16_t porcentajeHumedad;
  
  mesuraSalinidad(&voltsSalinidad,&conductanciaSalinidad,&porcentajeSalinidad);
  mesuraHumedad(&lecturaHumedad,&porcentajeHumedad);

  //imprimir resultados
  Serial.println("Mesura salinidad---------------");
  Serial.print(voltsSalinidad); Serial.println(" V, "); //Serial.println(stdev); 
  Serial.print(conductanciaSalinidad); Serial.println(" micro siemens");
  Serial.print(porcentajeSalinidad);Serial.println("%");

  Serial.println("Mesura humedad---------------");
  Serial.print("AIN0: ");
  Serial.println(lecturaHumedad);
  Serial.print("HR (%): ");
  Serial.println(porcentajeHumedad);
  
  delay(2000);//esperamos 2 segundos por mesura
}// ()
