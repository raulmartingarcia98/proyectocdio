//-------------
//
//  salinidad.h temperatura.h humedad.h
//  todas las clases de los sensores analogicos
//
//--------------

#ifndef SALINIDAD_H
#define SALINIDAD_H
// para evitar error de duplicidad de librerias del ads
#if !defined(ADS)
#define ADS
#include <Adafruit_ADS1015.h>
#endif

class Salinidad{

    private:
        int powerPin;
        int analogicPin;
        
        double valorCienPorCiento;
        double valorCeroPorCiento;
        
        double porcentaje;
        double conductancia;
        double volts;
        
        Adafruit_ADS1115 ads1115;
        
        void salinity_reading_stats( int , float *, float * );
        double getConductanciaUS(const double);
        double porcentajeSalinidad(const double);

    public:
        Salinidad();
        Salinidad(Adafruit_ADS1115, const int, const int , const double, const double);
        void medir();
        double getPorcentaje();
        double getConductancia();
        double getVolts();
    
};
#endif
