
//Autor: Grupo 11.
//Fecha: 22/10/2019.
//Descripción: Codigo completo del Arduino.

//--------------------------------
//----------LIBRERIAS-------------

#define ADS
#include <Adafruit_ADS1015.h>


#include "humedad.h"
#include "temperatura.h"
#include "salinidad.h"

//--------------------------------
//----------CONSTANTES------------

Adafruit_ADS1115 ads1115(0x48); // construct an ads1115 at address 0x48
Temperatura t;
Salinidad s;
Humedad h;
#define SALINITY_POWER_PIN 5 // Digital I/O pin


//--------------------------------
//----------SETUP-----------------

void setup() {

  Serial.begin(9600);
  Serial.println("Inicializando...");
  ads1115.begin(); //Initialize ads1115
  Serial.println("Ajustando la ganancia...");
  ads1115.setGain(GAIN_ONE);

  Serial.println("Tomando medidas del canal AIN0");
  Serial.println("Rango del ADC: +/- 4.096V (1 bit=2mV)");

  pinMode(SALINITY_POWER_PIN, OUTPUT);//configurar salida al pin de salinidad

  // inicializar los objetos de los sensores
  t = Temperatura(ads1115,2,0.0348,0.786,0.8);
  s = Salinidad(ads1115,SALINITY_POWER_PIN,1,1.6,2.3);
  h = Humedad(ads1115,20300, 11800);
}

//--------------------------------
//----------LOOP------------------

void loop() {
    // llamar a las funciones medir
  t.medir();
  s.medir();
  h.medir();
  
  //imprimir resultados

  Serial.println("Mesura salinidad---------------");
  Serial.print(s.getVolts()); Serial.println(" V, "); //Serial.println(stdev); 
  Serial.print(s.getConductancia()); Serial.println(" micro siemens");
  Serial.print(s.getPorcentaje());Serial.println("%");
  Serial.println("Mesura humedad---------------");
  Serial.print("HR (%): ");
  Serial.println(h.getPorcentaje());
  Serial.println("Mesura Temperatura---------------");
  Serial.println(t.getGrados());

    // esperamos para las suiguientes mesuras
  delay(2000); // 2 segundos
  ESP.deepSleep(3000000);// dormir 10 segundos (tiempo en microsegundos)
}// ()
