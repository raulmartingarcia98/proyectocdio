// ---------------------------------------------------
//
// Sensor.cpp
//
// ---------------------------------------------------
#include "salinidad.h"
#include "funciones.h"

// constructor por defecto
Salinidad::Salinidad(){}

// constructor parametrizado
// ads, el pin de alimentacion, el pin analogico, y los valores 0 por ciento y 100 por ciento para la calibracion
Salinidad::Salinidad(Adafruit_ADS1115 ads1115v, const int powerPinV, const int analogicPinV ,const double valor0v,const double valor100v)
    : ads1115(ads1115v),valorCeroPorCiento(valor0v), valorCienPorCiento(valor100v),
    powerPin(powerPinV),analogicPin(analogicPinV)
{}

//
// medir()-> Z,Z,Z
//
void Salinidad::medir(){
  //int salinity_input_pin = 1; //Pin A1 Salinidad
  int nave = 20; // 20 lecturas
  float ave, stdev;
  salinity_reading_stats(nave, &ave, &stdev);
  // pasamos el resultado a volts
  (*this).volts = byteToVoltsGanancia1(ave);
  (*this).conductancia = getConductanciaUS((*this).volts);
  (*this).porcentaje = porcentajeSalinidad((*this).volts);
  
  
}
//
// Z -> porcentajeSalinidad() -> Z
//
double Salinidad::porcentajeSalinidad(double voltajeAnalogico){ //Medida salinidad en función del voltaje
  double intervaloVoltios = 1;  //Intérvalo existente entra el valor 100(2.3v) y 0(1.6v)
  if (voltajeAnalogico <= (*this).valorCeroPorCiento){
    return 0.0;
  }else if (voltajeAnalogico >= (*this).valorCeroPorCiento){
    return 100.0;
  }else{
    return (voltajeAnalogico-1.6)/0.007; 
  }
  
}// ()

//
// Z -> getConductanciaUS() -> Z
// devuelve la conductancia en micro siemens
//
double Salinidad::getConductanciaUS(double voltajeAnalogico){
  // calculamos la resistencia aillada de la formula del divisor de tension aplicado en la resistencia
  // de 10 k ohmios
  double resistencia = (3.33*10000-voltajeAnalogico*10000)/voltajeAnalogico;
  double conductancia = 1/resistencia;
  return conductancia*1000000;
}//()

///------------------------------
/// Calcula la media y la desviacion estandard de las medidad
/// N,N,N -> salinity_reading_stats() -> R,R
///------------------------------
void Salinidad::salinity_reading_stats( int nave, float *ave, float *stdev ) {
 int buffer_length = 100; // Size of array to store readings for computation of ave and stdev
 // Reduce BUFFER_LENGTH to save memory if statistics are OK
 // with smaller sample size
 int i, n;
 float sum;
 float reading[buffer_length]; // Array para guardar las lecturas

 n = min( nave, buffer_length ); // Make sure we don't over-run the data buffer 
 
 // -- Guardar lecturas en el array
 for ( i=0; i<n; i++ ) {
  
   digitalWrite( (*this).powerPin, HIGH ); // Supply power to the sensor
   delay(100); // Wait for sensor to settle
   int16_t adc0 = ads1115.readADC_SingleEnded((*this).analogicPin);// el canal donde se va a leer (A0...A3)
   reading[i] = (float) (adc0*100)/100;
   digitalWrite( (*this).powerPin, LOW ); // Turn off power to the sensor
   delay(10); // esperar entre lecturas
 }// for
 
 // -- Calcular la media y la desviacion tipica.
 for ( sum=0.0, i=0; i<n; i++ ) {
    sum += reading[i];
 }// for
 
 *ave = sum/float(nave);

 for ( sum=0.0, i=0; i<n; i++ ) {
   sum += pow(reading[i] - *ave, 2);
 }// for
 
 *stdev = sqrt( sum/float(n-1) );
 
}// ()

// getters de los atributos del sensor de salinidad
double Salinidad::getPorcentaje(){return (*this).porcentaje;}
double Salinidad::getConductancia(){return (*this).conductancia;}
double Salinidad::getVolts(){return (*this).volts;}
