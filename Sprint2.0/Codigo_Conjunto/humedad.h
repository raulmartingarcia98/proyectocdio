//-------------
//
//  humedad.h 
//  clase para el sensor de humedad
//
//--------------
#ifndef HUMEDAD_H
#define HUMEDAD_H
// para evitar error de duplicidad de librerias del ads
#if !defined(ADS)
#define ADS
#include <Adafruit_ADS1015.h>
#endif
class Humedad{

    private:
        int valorCienPorCiento;
        int valorCeroPorCiento;
        double porcentaje;
        Adafruit_ADS1115 ads1115;

    public:
        Humedad();
        Humedad(Adafruit_ADS1115, const int, const int);
        void medir();
        double getPorcentaje();
    
};

#endif
