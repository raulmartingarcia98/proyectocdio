#ifndef FUNCIONES_H
#define FUNCIONES_H
//
// pasa de byte a volts sabiendo que la ganancia del ads esta
// configurada a uno, por lo tanto el maximo son 4.096 volts
// Se aplica una regla de 3
// Z->byteToVolts()->R
//

double byteToVoltsGanancia1(double value){
  return (value*4.096)/32767;
}


//
// calcula el porcentaje respecto a dos valores
// R -> porecentajeSalinidad() -> R
//
double calcularPorcentaje(double entrada, double valorMax, double valorMin){ //Medida salinidad en función del voltaje
  if (entrada <= valorMax){
    return 0.0;
  }else if (entrada >= valorMin){
    return 100.0;
  }else{
    return entrada;
  }
  
}// ()

#endif
