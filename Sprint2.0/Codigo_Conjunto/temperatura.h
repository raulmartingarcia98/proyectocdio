//-------------
//
//  temeperatura.h 
//  clase para el sensor de temperatura
//
//--------------

#ifndef TEMPERATURA_H
#define TEMPERATURA_H
// para evitar error de duplicidad de librerias del ads
#if !defined(ADS)
#define ADS
#include <Adafruit_ADS1015.h>
#endif

class Temperatura{

    private:

        int analogicPin;
        double grados;
        double pendiente;
        double terminoIndependiente;
        double ajuste;
        Adafruit_ADS1115 ads1115;
        double volts;

    public:
        Temperatura();
        Temperatura(Adafruit_ADS1115,const int, const double, const double,const double);
        void medir();
        double getGrados();
    
};
#endif
